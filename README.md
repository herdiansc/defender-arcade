# Defender Arcade
This is an app to find minimum arcade needed given a list of play time period.
## Problem
Armillary employees love playing video games, so they have Defender Arcade within the company (Work hard-Play hard). Since everyone is quite busy with work, everyone has provided the time (start time and finish time) when he or she wants to play games. If play time of two employees overlap then they start fighting and stop working. The boss got to know about this situation and asked you to help him by calculating the minimum number of Defender Arcades needed so that every employee can play during their specified time. 
Note: If one employee is leaving and at the same time another employee is starting then only one Arcade is needed.

## Requirement
- Golang: Please you can use this url https://golang.org/doc/install to install go on your machine.

## How to install
- Clone this repo to your $GOPATH/src directory

## How to Run
- Build the app to create executable file with command `go build .`
- Enter app root directory
- Execute command `./defender-arcade <input_filename>` for example `./defender-arcade DefenderArcade/input1.txt`

## How to Build
- Execute command `go build .`

## How to Run Unit Test
- Enter app root directory
- Execute command `go test ./...`

